java -Xmx2048m -cp bin\;lib\* nl.inl.blacklab.tools.IndexTool ^
	create ^
	%1 %2 %3 %4 %5 %6 %7 %8 %9 ^
	edu.northwestern.at.blacklab.indexers.DocIndexerAdornedXML
